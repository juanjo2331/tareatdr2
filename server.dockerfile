FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive
WORKDIR /tmp

RUN apt update
RUN apt install Bison
RUN apt install flex
RUN apt install pandoc


RUN axel https://dovecot.org/releases/2.3/dovecot-2.3.16.tar.gz \
&& tar dovecot-2.3.16.tar.gz \
&& cd tmp/dovecot-2.3.16.tar.gz \
&& ./configure \
&& make \
&& make install 